# Mapa 1
## Computadoras Electrónicas
```plantuml
@startmindmap
* Computadoras Electrónicas
	*[#7FFFD4] Harvard Mark l
		* Contruida por IBM en 1944, durante la 2da Guerra Mundial.
		* Tenia 765 000 componentes.
			* Uno de ellos eran sus 3500 relés.
			* Los cuales fallaba 1 al dia, esto provocaba que al realizar\ncalculos complejos llevaran varios dias.
		* 80 Km de cable, eje de 15m que mantenia en sincronia al dispositivo\ny utilizaba un motor de 5 HP.
		* Usos que se le dio:
			* Simulaciones.
			* Calculos para el proyecto Manhattan
				* Consistió en el desarrollo de la bomba atómica.
		* Podria realizar distintas operaciones tales como:
			* 3 sumas o restas por segundo.
			* Multiplicación en 6 segundos.
			* División en 15 segundos.
			* Operaciones ya complejas, en minutos.
		* Dato interesante
			* Debido a que las computadoras eran inmensamente grandes y calientes,\natraian insectos.
			* En Septiembre de 1947, la Harvard Mark 1 presento fallos el cual fue\nprovocado por una polilla muerta en uno de los relés.
			* Grace Hopper, cientifica de computación que trabajo en la Mark menciono que\ncuando algo salia mal con una computadora decian que tenian bichos (bugs)
				* De aqui surgio el termino "bugs", para referirse a los errores\nde las computadoras o de programación.
	*[#FFA07A] John Ambrose Fleming
		* En 1904, con el objetivo de corregir los problemas de los relés\ndesarrollo un componente electrico llamado "Válvula Termoiónica".
			* Esta conformado por un filamento, 2 electrodos (Cátodo y Ánodo) dentro de un bulbo de cristal.
			* Funcionamiento:
				* Cuando el filamento se enciende y calienta, permite el flujo de electrones desde ánodo hacia el cátodo.
					* Este proceso se llama Emisión Termodinámica.
					* Permite el flujo de la corriente en una sola dirección.
					* El cual ya es conocido como "diodo".
	*[#FF1493] Lee De Forest
		* En 1906, agrego un tercer electrodo llamado "electrodo de control",\n el cual estaba situado entre el ánodo y cátodo del diseño de Fleming.
			* Al aplicar una carga positiva (+) o negativa (-) en el electrodo de control,\npermitia el flujo o detenia la corriente.
			* Logrando asi la misma funcionalidad que los relés, pero sin el problema de las partes moviles.
				* Habia mas confiabilidad, ya que se dañaban\nmenos con el uso y mayor velocidad.
			* Estos tubos tenian la capacidad de cambiar de estado miles de veces por segundo.
			* Fueron la base para la radio, los telefonos de larga distancia, y muchos otros\ndispositos electronicos por mas de 50 años.
			* Sin embargo, estos tubos de vacio no eran perfectos. Eran frágiles y\nse podian quemar como los focos o bulbos de luz.
	*[#FFD700] 1940
		* El hecho de reemplazar los relés por los tubos de vacio, supujo el hito que dejo atras\nlas computadoras electromecánicas e inicio la era las compuatadoras electronicas.
	*[#9ACD32] Colossus Mark 1
		* Se le aplico el primer uso masivo de los tubos de vacio.
		* Diseñada por el ingeniero Tommy Flowers y finalizada su construccion hasta diciembre de 1943.
		* Instalada en Bletchley Park, Reino Unido. Ayudaba a decodificar\nlas comunicaciones Nazis.
		* La primera version del Colossus tenia 1600 tubos de vacio.
			* Se llegaron a contruir un total de 10 Colossus\npara ayudar en la decodificación.
		* Es conocida como la 1ra computadora electronica programable.
	*[#FF4500] ENIAC
		* Que sus siglas se refieren a Calculadora Integradora Numérica Electrónica.
		* Contruidad en 1946, en la Universidad de Pennsylvania.
			* Diseñada por John Mauchly y J. Presper Eckert.
		* Fue la 1ra computadora electrónica programable de propósito general.
			* Podia realizar 5000 sumas y restas de 10 dígitos por segundo.
		* Operó por 10 años, y se estima que realizo mas operaciones aritméticas\nque toda la humanidad hasta ese momento.
			* Debido a sus constantes fallas en los tubos de\nvacio, era operacional durante medio dia.
	*[#FFD700] 1947
		* Los cientificos del Laboratorio Bell, John Bardeen,\nWalter Brattain y William Shockley crearon el "transistor".
			* Con la invención de este, inicio una nueva era de la computación.
			* Un transistor cumple la misma funión que un rele o un tubo de vacio.
				* Es decir, un interruptor electrico.
			* Esta hecho de Silicio, que es su estado puro es aislante.
				* No permite el paso de corriente electrica.
			* Gracias a un paso llamado "Dopaje", se le agregan otros elementos\nal silicio los cuales permiten la conduccion de la corriente.
				* Dependiendo del elemento que se le agregue puede\ntener defecto o exceso de electrones.
			* Conformado por 3 capaz de estos tipos de silicio.
				* Cada uno representa un conector, llamados "Colector", "Emisor" y "Base".
				* Cuando hay un flujo de corriente entre la base y el emisor\npermite que exista un flujo entre el colector y el emisor.
					* Cumpliendo su objetivo de comportarse\ncomo interruptor electrico.
			* Podia cambiar de estado 10000 Hertz por segundos.
			* Eran solidos, mucho mas resistentes.
			* Se podian hacer mas pequeños que los relés y los tubos de vacios.
		* Gracias a los transistores se pudieron\nconstruir computadoras mucho mas baratas.
			* Como la IBM-608 que fue lanzada en 1957.
	* La primera computadora disponible comercialmente\nbasada completamente en transistores.
		* Podia realizar 4500 sumas, 80 divisiones o multiplicaciones por segundo.
		* IBM comenzo a utilizar transistores en todos sus productos.
			* Acercando las computadoras\nen las oficinas y hogares.
	*[#FF6347] Actualidad
		* Los transistores utilizados en las computadoras\ntiene un tamaño menor a 50 nanometros.
		* Son rapidos, pueden cambiar de estado\nmillones de veces por segundo.
		* Pueden funcionar por decadas.
@endmindmap
```
# Mapa 2
## Arquitectura Von Neumann y Arquitectura Harvard
```plantuml
@startmindmap
* Arquitectura de Computadoras
	*[#7FFFD4] Ley de Moore
		* Establece que la velocidad del procesador o el poder\nde procesamiento total de las computadoras se duplica\ncada 2 meses.
		* Electronica
			* El numero de transistores por chip se duplica cada año
		* Perfomance
			* Se incrementa la velocidad del procesador.
			* Se incrementa la capacidad de la memoria.ç
	*[#ADFF2F] Funcionamiento de una\ncomputadora
		* La primera computadora, eran sistemas cableados.
			* Datos > Secuencia de funciones aritmeticas/lógicas > Resultados
			* Programación mediante hardware
		* Sistema actual
			* Datos > (Intérprete de instrucciones > Señales de control) > Secuencia\nde funciones aritméticas/lógicas > Resultados
			* Programación mediante Software
	*[#FFA07A] Von Neumann
		* Arquitectura moderna, que utilizamos a diario.
		* 3 partes fundamentales para un computador digital electrónico
			* CPU: contiene una unidad de control, una Unidad Aritmetica Lógica y Registros.
			* Memoria principal: la cual puede almacenar tanto instrucciones como datos.
			* Sistema de Entrada/Salida
		* Este modelo describe que...
			* Los datos y programas se almacenan en una misma\nmemoria de lectura-escritura.
			* Los contenidos de esta memoria se acceden\nindicando su posicion sin importar su tipo.
			* La ejecución es en secuencia.
			* Es mediante representacion binaria.
		* Modelo de bus
			* Es un dispositivo en comun entre dos o mas dispositivos.
			* Cada linea puede transmitir señales que representan 1 y 0,\nen secuencia, de a una señal por unidad de tiempo.
			* Existen varios tipos de buses que realizan la tarea de interconexión\nentre las distintas partes del computador.
				* Bus de sistema: comunica el procesador, memoria y E/S.
				* Bus de datos
				* Bus de direcciones
				* Bus de control
		* Instrucciones
			* La función de una computadora es la ejecución de programas.
				* Estos se encuentran localizados en memoria\ny consisten de instrucciones.
			* La CPU es quien se encarga de ejecutar dichas instrucciones, a grandes velocidades.
				* A esto se le llama Ciclo de instrucción.
			* Las intrucciones consisten de secuencias de 1 y 0.
	*[#9370D8] Havard
		* Se referia a las arquitecturas de computadoras que utilizaban\ndispositivos de almacenamientos fisicamente separados\npara las instrucciones y para los datos.
		* De igual forma hay 3 partes fundamentales para\nun computador digital electrónico
			* CPU: contiene una unidad de control, una Unidad Aritmetica Lógica y Registros.
			* Memoria principal: la cual puede almacenar tanto instrucciones como datos.
			* Sistema de Entrada/Salida
		* Memorias
			* Fabricar memorias mucho mas rapidas tenia un precio muy alto.
			* La solución fue proporcionar una pequeña cantidad de memoria\nmuy rapida conocida con el nombre de CACHE.
		* Solucion Harvard
			* Las instrucciones y los datos se almacenan en\ncachés separadas para mejorar el rendimiento.
		* Procesador
			* Unidades funcionales principales
				* Unidad de Control (UC) y Unidad aritmética y lógica (AUL).
			* La UC lee la instruccion de memoria de instrucciones.
			* Genera las señales de control para obtener los operandos\nde memoria de datos.
			* Ejecuta la instruccion mediante la ALU almacenando el resultado\nen la memoria de datos.
		* Memoria de Instrucciones
			* Es donde se almacenan las instrucciones del programa que debe ejecutar\nel microcontrolador.
			* Se implementa utilizando memorias no volátiles: ROM, PROM, EPROM, EEPROM o flash.
		* Memoria de Datos
			* Se almacenan los datos utilizados por los programas.
			* Los datos varian continuamente, por lo tanto utilizan memoria RAM\nsobre la cual se pueden realizar operaciones de\nlectura y escritura.
		* ¿Dónde se utilizan los\nmicrocontroladores?
			* En desarrollo de productos o sistemas de propósito especifico como\nelectrodomésticos, telecomunicaciones, automoviles, mouse, impresoras, en\nprocesamiento de audio y video, hasta robotica, entre otras cosas.
@endmindmap
```
# Mapa 3
## Basura Electrónica
```plantuml
@startmindmap
* Basura Electrónica
	*[#6495ED] Centro de reciclaje e-end
		* Ubicado en E.U, Meryland
		* Uno de los materiales que mayor se extrae aqui\n es el oro.
			* Los cuales son encontrados, en terminales,\ncircuitos y procesadores.
		* Otros metales como el cobre, plata y cobalto son encontrados\ncomunmente en los circuitos.
		* En estos centros de reciclaje, el proceso es costoso\ny complicado, pero todo es de manera segura.
		* Otra razon de realizar un reciclaje aqui, es la proteccion a tus datos.
			* Ya que, se olvida que a veces la informacion personal no\nse borra y los discos duros pueden ser reparados para fines criminales.
				* Robo de identidad
		* Asi que se ocupa, una maquina con imanes gigantes.
			* Borra todos lo datos que siguen guardados\nen estos aparatos.
			* O se destruyen por completo, en grandes trituradoras.
	*[#DC143C] Reciclaje en México
		* Techemet
			* El primer paso es la recepción del material eléctrico,\ndespués juntarlo y clasificarlo, así como pesarlo\npara calcular el pago por el producto a reciclar.
			* El segundo paso es desarmarlo y clasificarlo de acuerdo a sus componentes.
			* El tercer paso es la segregación y la limpieza de todas las piezas.
			* A continuación se almacena con su correspondiente clasificación.
			* Y finalmente se exporta para que la matriz realice\nel último trabajo de fundición y recuperación\nde los metales.
			* Brinda beneficios económicos a los individuos o empresas\ny es accesible gracias a los diferentes puntos de recolección\nde desechos en todo México.
		* Monterrey
			* En algunos centros de reciclaje, la mayoria de la basura electronica llega por donaciones.
				* Computadoras de distintas generaciones
				* Proyectores de video
				* Consolas de videojuegos
				* Estereos
				* Reproductores de video o casets
				* Laptops
				* Monitores
				* Y demas aparatos electrónicos.
			* Se obtienen sus componentes que se puedan reutilizar o realizar totalmente su reciclaje.
	*[#DAA520] ¿Sabias que?
		* El 70% de las toxinas que se desprenden de los\ntiraderos de basura provienen de los desechos electrónicos.
			* Es debido a los cambios rápidos en el mundo de la tecnología,\nel abaratamiento e incluso la obsolescencia han dado\nlugar al crecimiento de los ordenadores y otros\ncomponentes electrónicos.
		* Los equipos obsoletos que ya no se utilizan en casa o empresa\ntales como celulares, televisiones, centrales telefónicas,\nequipos de computación entre otros, son una importante\nfuente de materias primas secundarias.
			* Por eso es importante depositarlos en empresas de reciclaje\nconfiables y responsables en el manejo de los desechos y\nasegurar la reutilización de sus componentes.
			* El aparato electrónico que ya pasó su vida útil se puede\nconsiderar como un residuo electrónico y se puede reciclar.
		* En México anualmente se desechan entre 150 y 180 mil toneladas de\nbasura electrónica entre equipos de cómputo, televisiones, teléfonos,\ncelulares, aparatos de audio y vídeo, sin regulación y control.
	*[#90EE90] Lotes Electrónicos
		* Hay organizaciones, que se dedican a comprar lotes de alguna empresa.
			* Comunmente reciben computadoras analogicas, laptops, CPU's, etc.
			* Algo bastante destacable son los dicos duros solidos.
				* Aunque estos sean caros o nuevos, no se\nlos pueden quedar y tienen que ser destruidos por\nel compromiso a la empresa.
		* Para algunas empresas, los equipos o componentes electronicos son obsoletos.
			* Mayormente son laptops, cargadores, pilas, tarjetas madres, audifonos, mouse's, memorias RAM.
			* Lo bueno es que se pueden reparar o vender por piezas a distintos lugares.
		* En resumen, para algunos es buena fuente de recursos economicos.
	*[#DDA0DD] Costa Rica
		* Empresa Servicios Ecológicos
			* Tienen relaciones con distintas organizaciones, escuelas o\ninstituciones gubernamentales en proyectos de reciclaje.
			* Aqui se lleva un porceso de clasificacion, desmantelamiento, selección y una vez listos los\nmateriales son enviados al procesador final.
				* Ejemplo es desarmar los aparatos electronicos y extraer sus componentes.
				* En cuanto a la selecion, un ejemplo el cable es separado en cobre y plastico.
				* Otra forma de reciclaje, es clasificar las tarjetas madres,\nlas pesan y luego son desarmadas.
					* Toman el cobre y el bronce, algunas otras partes y\nesperan a que algun cliente las compren.
			* Es una fuente de trabajo para 150 personas y progreso ecológico para el pais.
	*[#DC143C] Factores negativos
		* La mayor parte de la basura electrónica\nno termina en fabricas de reciclaje.
		* Muchas veces la basura electronica no es reciclada de manera segura.
			* Los cuales provocan daños al medio ambiente, como a las personas.
		* Enormes cantidades de basura electrónica es exportada de manera ilegal.
			* Paises como México, China o India.
			* En donde son desambrados para extraer los metales preciosos.
			* Usan metodos primitivos y mas baratos, sin el equipo adecuado.
				* Lo cual provoca, que al derretir los electrónicos\ninhalen todos los vapores tóxicos.
				* Sufren graves problemas de salud, como la tuberculosis y cancer. 
 			* Luego votan los residuos quemados y los quimicos,\ny estos terminan en rios.
				* Los cuales contaminan tanto el agua como el aire.
		* Desgraciadamente con frecuencia son depositados en la basura sin ser aprovechados.
		* La tecnología se vuelve obsoleta con rapidez pues aún no se encuentra\nlas técnicas que permitan contrarrestar este fenómeno global.
@endmindmap
```
